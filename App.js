import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={{ flex: 1, backgroundColor: 'steelblue', marginTop: 20, alignItems: 'center' }}><Text style={{fontWeight:'bold', fontSize:16}}>Test</Text></View>
      <View style={{ flex: 2, backgroundColor: 'skyblue' }}>
        <TextInput style={{ height: 40, backgroundColor: '#fff', borderColor: 'gray', borderWidth: 1 }} value='Input'></TextInput>
      </View>
      <View style={{ flex: 3, backgroundColor: 'powderblue' }}><Button title='click' onPress={() => alert('Hello')}></Button></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'steelblue',
    flex: 1,
    alignItems: 'stretch',
  },
});
